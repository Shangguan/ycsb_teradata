-- drop everything
DROP TABLE IF EXISTS usertable2;
DROP HDFSSTORE IF EXISTS hdfsstore2;
DROP DISKSTORE IF EXISTS hdfsStoreDisk2;
DROP TABLE IF EXISTS usertable;
DROP HDFSSTORE IF EXISTS hdfsstore;
DROP DISKSTORE IF EXISTS hdfsStoreDisk;


-- create stores
-- CREATE DISKSTORE hdfsStoreDisk autocompact false ('hdfsStoreData');

-- CREATE HDFSSTORE hdfsstore NAMENODE 'hdfs://hdm1.gphd.local:8020' HOMEDIR '/gemfirexd_data_hdfsstore' DISKSTORENAME hdfsStoreDisk MINORCOMPACT true MINORCOMPACTIONTHREADS 3 MAJORCOMPACT false QUEUEPERSISTENT true BATCHSIZE 128 MAXQUEUEMEMORY 512;

CREATE HDFSSTORE hdfsstore NAMENODE 'hdfs://hdm1.gphd.local:8020' HOMEDIR '/gemfirexd_data_hdfsstore';

-- CREATE DISKSTORE hdfsStoreDisk2 autocompact false ('hdfsStoreData2');

-- CREATE HDFSSTORE hdfsstore2 NAMENODE 'hdfs://hdm1.gphd.local:8020' HOMEDIR '/gemfirexd_data_hdfsstore2' DISKSTORENAME hdfsStoreDisk2 MINORCOMPACT true MINORCOMPACTIONTHREADS 3 MAJORCOMPACT false QUEUEPERSISTENT true BATCHSIZE 128 MAXQUEUEMEMORY 512;

CREATE HDFSSTORE hdfsstore2 NAMENODE 'hdfs://hdm1.gphd.local:8020' HOMEDIR '/gemfirexd_data_hdfsstore2';

-- create tables
CREATE TABLE usertable (YCSB_KEY VARCHAR(100) PRIMARY KEY, FIELD0 VARCHAR(100), FIELD1 VARCHAR(100), FIELD2 BIGINT, FIELD3 BIGINT, FIELD4 VARCHAR(100), FIELD5 VARCHAR(100), FIELD6 VARCHAR(100), FIELD7 VARCHAR(100), FIELD8 VARCHAR(100), FIELD9 VARCHAR(100)) partition by (YCSB_KEY) buckets 113 redundancy 1 EVICTION BY CRITERIA (mod(FIELD2, 10)=1) EVICT INCOMING HDFSSTORE (hdfsstore);

-- CREATE TABLE usertable2 (YCSB_KEY VARCHAR(100) PRIMARY KEY, FIELD0 VARCHAR(100), FIELD1 VARCHAR(100), FIELD2 BIGINT, FIELD3 BIGINT, FIELD4 VARCHAR(100), FIELD5 VARCHAR(100), FIELD6 VARCHAR(100), FIELD7 VARCHAR(100), FIELD8 VARCHAR(100), FIELD9 VARCHAR(100)) partition by (field4) colocate with (usertable) buckets 113 redundancy 1 EVICTION BY CRITERIA (mod(FIELD2, 10)=1) EVICT INCOMING HDFSSTORE (hdfsstore2);

CREATE TABLE usertable2 (YCSB_KEY VARCHAR(100) PRIMARY KEY, FIELD0 VARCHAR(100), FIELD1 VARCHAR(100), FIELD2 BIGINT, FIELD3 BIGINT, FIELD4 VARCHAR(100), FIELD5 VARCHAR(100), FIELD6 VARCHAR(100), FIELD7 VARCHAR(100), FIELD8 VARCHAR(100), FIELD9 VARCHAR(100)) partition by (YCSB_KEY) colocate with (usertable) buckets 113 redundancy 1 EVICTION BY CRITERIA (mod(FIELD2, 10)=1) EVICT INCOMING HDFSSTORE (hdfsstore2);

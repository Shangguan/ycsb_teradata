-- load/insert
PUT INTO usertable VALUES('random PK','random string','random string',200,300,'random string','random string','random string','random string','random string','random string');

-- read/update mix (50:50), w/ HDFS
SELECT * FROM usertable --GEMFIREXD-PROPERTIES queryHDFS=true \n
WHERE YCSB_KEY='PK value';

UPDATE usertable
SET FIELD0='random string', FIELD1='random string', FIELD2=201, FIELD3=301, FIELD4='random string', FIELD5='random string', FIELD6='random string', FIELD7='random string', FIELD8='random string', FIELD9='random string'
WHERE YCSB_KEY='PK value';

-- read/update mix (50:50), in-memory only
SELECT * FROM usertable --GEMFIREXD-PROPERTIES queryHDFS=false \n
WHERE YCSB_KEY='PK value';

UPDATE usertable
SET FIELD0='random string', FIELD1='random string', FIELD2=201, FIELD3=301, FIELD4='random string', FIELD5='random string', FIELD6='random string', FIELD7='random string', FIELD8='random string', FIELD9='random string'
WHERE YCSB_KEY='PK value';

-- Lookup by PK only, w/ HDFS
SELECT * FROM usertable --GEMFIREXD-PROPERTIES queryHDFS=true \n
WHERE YCSB_KEY='PK value';

-- Lookup by PK only, in-memory only
SELECT * FROM usertable --GEMFIREXD-PROPERTIES queryHDFS=false \n
WHERE YCSB_KEY='PK value';

-- Query with filter (non-PK), w/ HDFS
SELECT * FROM usertable --GEMFIREXD-PROPERTIES queryHDFS=true \n
WHERE u.field0 = 'string value' AND u.field2 > 201 AND u.field2 < 299?
FETCH FIRST 100 ROWS ONLY;

-- Query with filter (non-PK), in-memory only
SELECT * FROM usertable --GEMFIREXD-PROPERTIES queryHDFS=false \n
WHERE u.field0 = 'string value' AND u.field2 > 201 AND u.field2 < 299?
FETCH FIRST 100 ROWS ONLY;

-- Query with aggregate (non-PK), w/ HDFS
SELECT u.field1, sum(u.field3) FROM usertable --GEMFIREXD-PROPERTIES queryHDFS=true \n
WHERE u.field0 = 'string value' AND u.field2 > 201 AND u.field2 < 299
GROUP BY u.field1 ORDER BY u.field1
FETCH FIRST 100 ROWS ONLY;

-- Query with aggregate (non-PK), in-memory only
SELECT u.field1, sum(u.field3) FROM usertable --GEMFIREXD-PROPERTIES queryHDFS=false \n
WHERE u.field0 = 'string value' AND u.field2 > 201 AND u.field2 < 299
GROUP BY u.field1 ORDER BY u.field1
FETCH FIRST 100 ROWS ONLY;

-- Query with join (non-PK), w/ HDFS
SELECT u.ycsb_key, v.ycsb_key, u.field1, v.field1
FROM usertable u JOIN usertable2 --GEMFIREXD-PROPERTIES queryHDFS=true \n
ON u.ycsb_key = v.field4
WHERE u.field0 = 'string value' AND u.field2 > 201 AND u.field2 < 299
ORDER BY u.field1, v.field0
FETCH FIRST 100 ROWS ONLY;

-- Query with join (non-PK), in-memory only
SELECT u.ycsb_key, v.ycsb_key, u.field1, v.field1
FROM usertable u JOIN usertable2 --GEMFIREXD-PROPERTIES queryHDFS=false \n
ON u.ycsb_key = v.field4
WHERE u.field0 = 'string value' AND u.field2 > 201 AND u.field2 < 299
ORDER BY u.field1, v.field0
FETCH FIRST 100 ROWS ONLY;
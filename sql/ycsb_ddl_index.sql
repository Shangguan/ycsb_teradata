-- create index
CREATE INDEX field0_usertable_idx ON usertable (FIELD0);
CREATE INDEX field1_usertable_idx ON usertable (FIELD1);
CREATE INDEX field2_usertable_idx ON usertable (FIELD2);
CREATE INDEX field3_usertable_idx ON usertable (FIELD3);
CREATE INDEX field0_field2_usertable_idx ON usertable (FIELD0, FIELD2);
CREATE INDEX field1_field0_field2_usertable_idx ON usertable (FIELD1, FIELD0, FIELD2);
CREATE INDEX field0_usertable2_idx ON usertable2 (FIELD0);
CREATE INDEX field1_usertable2_idx ON usertable2 (FIELD1);
CREATE INDEX field2_usertable2_idx ON usertable2 (FIELD2);
CREATE INDEX field3_usertable2_idx ON usertable2 (FIELD3);
#!/bin/bash

export RECORDCOUNT=10000000

echo -e "\033[1mBenchmark: GemFire XD with HDFS Persistence\033[0m"
echo -e "\033[1mCreating tables and indices\033[0m"
echo -e "\033[1m-------------------------------------------\033[0m"
gfxd run -file=/home/gpadmin/shangz/ycsb/sql/ycsb_ddl_hdfs.sql -client-bind-address=hdw8.gphd.local -client-port=1527
gfxd run -file=/home/gpadmin/shangz/ycsb/sql/ycsb_ddl_index.sql -client-bind-address=hdw8.gphd.local -client-port=1527

echo -e "\033[1mLoading data\033[0m"
echo -e "\033[1m-------------------------------------------\033[0m"
/home/gpadmin/shangz/ycsb/run/gfxd_ycsb_hdfs_load.sh

echo -e "\033[1mRunning benchmark\033[0m"
echo -e "\033[1m-------------------------------------------\033[0m"
# /home/gpadmin/shangz/ycsb/run/gfxd_ycsb_hdfs_q1.sh
/home/gpadmin/shangz/ycsb/run/gfxd_ycsb_hdfs_q2.sh
/home/gpadmin/shangz/ycsb/run/gfxd_ycsb_hdfs_q5.sh
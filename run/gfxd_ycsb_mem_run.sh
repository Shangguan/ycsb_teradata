#!/bin/bash

export RECORDCOUNT=10000000

echo -e "\033[1mBenchmark: GemFire XD In-Memory Only\033[0m"
#echo -e "\033[1mCreating tables and indices\033[0m"
#echo -e "\033[1m-------------------------------------------\033[0m"
#gfxd run -file=/home/gpadmin/shangz/ycsb/sql/ycsb_ddl_mem.sql -client-bind-address=hdw8.gphd.local -client-port=1527
#gfxd run -file=/home/gpadmin/shangz/ycsb/sql/ycsb_ddl_index.sql -client-bind-address=hdw8.gphd.local -client-port=1527

#echo -e "\033[1mLoading data\033[0m"
#echo -e "\033[1m-------------------------------------------\033[0m"
#/home/gpadmin/shangz/ycsb/run/gfxd_ycsb_mem_load.sh

echo -e "\033[1mRunning benchmark\033[0m"
echo -e "\033[1m-------------------------------------------\033[0m"
#/home/gpadmin/shangz/ycsb/run/gfxd_ycsb_mem_q1.sh
#/home/gpadmin/shangz/ycsb/run/gfxd_ycsb_mem_q2.sh
#home/gpadmin/shangz/ycsb/run/gfxd_ycsb_mem_q3.sh
#/home/gpadmin/shangz/ycsb/run/gfxd_ycsb_mem_q4.sh
/home/gpadmin/shangz/ycsb/run/gfxd_ycsb_mem_q5.sh
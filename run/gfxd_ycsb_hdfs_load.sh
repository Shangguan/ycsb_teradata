#!/bin/bash

export YCSB_HOME=/home/gpadmin/shangz/ycsb/ycsb-0.1.4

DATE=`date +%y%m%d`
if [ ! -d  $PWD/$DATE ]; then
    mkdir -p $PWD/$DATE
fi

# load two tables
$YCSB_HOME/bin/ycsb load gemfirexd -p db.driver=com.pivotal.gemfirexd.jdbc.EmbeddedDriver -p 'db.url=jdbc:gemfirexd:;locators=hdw8.gphd.local[10101];host-data=false;conserve-sockets=false' -p generatequerydata=true -p generatetwotables=true -p workload=com.yahoo.ycsb.workloads.CoreWorkload -p recordcount=$RECORDCOUNT -threads 32 -s > $PWD/$DATE/gfxd-hdfs-load_2t.log

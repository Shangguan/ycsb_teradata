#!/bin/bash

gpscp -f /home/gpadmin/shangz/hostfile_gemxd_locators /home/gpadmin/shangz/ycsb/run/gfxd_startlocator.sh =:/home/gpadmin
gpscp -f /home/gpadmin/shangz/hostfile_gemxd_locators /home/gpadmin/shangz/ycsb/run/gfxd_shutdownall.sh =:/home/gpadmin
gpscp -f /home/gpadmin/shangz/hostfile_gemxd_servers /home/gpadmin/shangz/ycsb/run/gfxd_startserver.sh =:/home/gpadmin

gpssh -f /home/gpadmin/shangz/hostfile_gemxd_locators -v -e 'chmod u+x gfxd_startlocator.sh'
gpssh -f /home/gpadmin/shangz/hostfile_gemxd_locators -v -e '$HOME/gfxd_startlocator.sh'

gpssh -f /home/gpadmin/shangz/hostfile_gemxd_servers -v -e 'chmod u+x gfxd_startserver.sh'
gpssh -f /home/gpadmin/shangz/hostfile_gemxd_servers -v -e '$HOME/gfxd_startserver.sh'

#!/bin/bash

export YCSB_HOME=/home/gpadmin/shangz/ycsb/ycsb-0.1.4

DATE=`date +%y%m%d`
if [ ! -d  $PWD/$DATE ]; then
    mkdir -p $PWD/$DATE
fi

echo -e "\033[1mWorkload: Lookup by PK only, one table (in-memory only), threads=32\033[0m"
echo -e "\033[1m-------------------------------------------\033[0m"
$YCSB_HOME/bin/ycsb run gemfirexd -p db.driver=com.pivotal.gemfirexd.jdbc.EmbeddedDriver -p 'db.url=jdbc:gemfirexd:;locators=hdw8.gphd.local[10101];host-data=false;conserve-sockets=false' -p query.hdfs=false -threads 32 -P $YCSB_HOME/workloads/gfxd_hdfs_workload_q2 -p recordcount=$RECORDCOUNT -p operationcount=16000000 -s > $PWD/$DATE/gfxd-mem-run_q2_t32.log
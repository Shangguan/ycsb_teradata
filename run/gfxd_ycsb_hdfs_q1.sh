#!/bin/bash

export YCSB_HOME=/home/gpadmin/shangz/ycsb/ycsb-0.1.4

DATE=`date +%y%m%d`
if [ ! -d  $PWD/$DATE ]; then
    mkdir -p $PWD/$DATE
fi

# transaction: Read/Update Mix (50:50), on one table, query HDFS, threads=2/4/8/16
echo -e "\033[1mWorkload B: Read/Update Mix (50:50), one table, query HDFS, threads=2\033[0m"
echo -e "\033[1m-------------------------------------------\033[0m"
$YCSB_HOME/bin/ycsb run gemfirexd -p db.driver=com.pivotal.gemfirexd.jdbc.EmbeddedDriver -p 'db.url=jdbc:gemfirexd:;locators=hdw8.gphd.local[10101];host-data=false;conserve-sockets=false' -p query.hdfs=true -threads 2 -P $YCSB_HOME/workloads/gfxd_hdfs_workload_q1 -s > $PWD/$DATE/gfxd-hdfs-run_a_t2.log

echo -e "\033[1mWorkload B: Read/Update Mix (50:50), one table, query HDFS, threads=4\033[0m"
echo -e "\033[1m-------------------------------------------\033[0m"
$YCSB_HOME/bin/ycsb run gemfirexd -p db.driver=com.pivotal.gemfirexd.jdbc.EmbeddedDriver -p 'db.url=jdbc:gemfirexd:;locators=hdw8.gphd.local[10101];host-data=false;conserve-sockets=false' -p query.hdfs=true -threads 4 -P $YCSB_HOME/workloads/gfxd_hdfs_workload_q1 -p operationcount=2000000 -s > $PWD/$DATE/gfxd-hdfs-run_a_t4.log

echo -e "\033[1mWorkload B: Read/Update Mix (50:50)y, one table, query HDFS, threads=8\033[0m"
echo -e "\033[1m-------------------------------------------\033[0m"
$YCSB_HOME/bin/ycsb run gemfirexd -p db.driver=com.pivotal.gemfirexd.jdbc.EmbeddedDriver -p 'db.url=jdbc:gemfirexd:;locators=hdw8.gphd.local[10101];host-data=false;conserve-sockets=false' -p query.hdfs=true -threads 8 -P $YCSB_HOME/workloads/gfxd_hdfs_workload_q1 -p operationcount=4000000 -s > $PWD/$DATE/gfxd-hdfs-run_a_t8.log

echo -e "\033[1mWorkload B: Read/Update Mix (50:50), one table, query HDFS, threads=16\033[0m"
echo -e "\033[1m-------------------------------------------\033[0m"
$YCSB_HOME/bin/ycsb run gemfirexd -p db.driver=com.pivotal.gemfirexd.jdbc.EmbeddedDriver -p 'db.url=jdbc:gemfirexd:;locators=hdw8.gphd.local[10101];host-data=false;conserve-sockets=false' -p query.hdfs=true -threads 16 -P $YCSB_HOME/workloads/gfxd_hdfs_workload_q1 -p operationcount=8000000 -s > $PWD/$DATE/gfxd-hdfs-run_a_t16.log

# transaction: Read/Update Mix (50:50), on one table, query in-memory only, threads=2/4/8/16
echo -e "\033[1mWorkload B: Read/Update Mix (50:50), one table, query in-memory only, threads=2\033[0m"
echo -e "\033[1m-------------------------------------------\033[0m"
$YCSB_HOME/bin/ycsb run gemfirexd -p db.driver=com.pivotal.gemfirexd.jdbc.EmbeddedDriver -p 'db.url=jdbc:gemfirexd:;locators=hdw8.gphd.local[10101];host-data=false;conserve-sockets=false' -p query.hdfs=false -threads 2 -P $YCSB_HOME/workloads/gfxd_hdfs_workload_q1 -s > $PWD/$DATE/gfxd-hdfs-run_a_m_t2.log

echo -e "\033[1mWorkload B: Read/Update Mix (50:50), one table, query in-memory only, threads=4\033[0m"
echo -e "\033[1m-------------------------------------------\033[0m"
$YCSB_HOME/bin/ycsb run gemfirexd -p db.driver=com.pivotal.gemfirexd.jdbc.EmbeddedDriver -p 'db.url=jdbc:gemfirexd:;locators=hdw8.gphd.local[10101];host-data=false;conserve-sockets=false' -p query.hdfs=false -threads 4 -P $YCSB_HOME/workloads/gfxd_hdfs_workload_q1 -p operationcount=2000000 -s > $PWD/$DATE/gfxd-hdfs-run_a_m_t4.log

echo -e "\033[1mWorkload B: Read/Update Mix (50:50), one table, query in-memory only, threads=8\033[0m"
echo -e "\033[1m-------------------------------------------\033[0m"
$YCSB_HOME/bin/ycsb run gemfirexd -p db.driver=com.pivotal.gemfirexd.jdbc.EmbeddedDriver -p 'db.url=jdbc:gemfirexd:;locators=hdw8.gphd.local[10101];host-data=false;conserve-sockets=false' -p query.hdfs=false -threads 8 -P $YCSB_HOME/workloads/gfxd_hdfs_workload_q1 -p operationcount=4000000 -s > $PWD/$DATE/gfxd-hdfs-run_a_m_t8.log

echo -e "\033[1mWorkload B: Read/Update Mix (50:50), one table, query in-memory only, threads=16\033[0m"
echo -e "\033[1m-------------------------------------------\033[0m"
$YCSB_HOME/bin/ycsb run gemfirexd -p db.driver=com.pivotal.gemfirexd.jdbc.EmbeddedDriver -p 'db.url=jdbc:gemfirexd:;locators=hdw8.gphd.local[10101];host-data=false;conserve-sockets=false' -p query.hdfs=false -threads 16 -P $YCSB_HOME/workloads/gfxd_hdfs_workload_q1 -p operationcount=8000000 -s > $PWD/$DATE/gfxd-hdfs-run_a_m_t16.log

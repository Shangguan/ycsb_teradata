#!/bin/bash

CLIENTPORT=1527
PEER_DISCOVERY_PORT=10101
GFXD_DATADIR=$HOME/gfxd
PATH=$PATH:/usr/local/gfxd/bin/

echo -e "\033[1mStopping GemFire XD servers\033[0m"
echo -e "\033[1m-------------------------------------------\033[0m"

gfxd shut-down-all -locators=$HOSTNAME[$PEER_DISCOVERY_PORT]

echo -e "\033[1mStopping GemFire XD locator service\033[0m"
echo -e "\033[1m-------------------------------------------\033[0m"

gfxd locator stop -dir=$GFXD_DATADIR/locator.$HOSTNAME
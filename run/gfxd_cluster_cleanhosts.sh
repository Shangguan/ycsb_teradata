#!/bin/bash

gpssh -f /home/gpadmin/shangz/hostfile_gemxd_locators -v -e 'rm -rf $HOME/gfxd'
gpssh -f /home/gpadmin/shangz/hostfile_gemxd_locators -v -e 'rm $HOME/gfxd_*.sh'
gpssh -f /home/gpadmin/shangz/hostfile_gemxd_locators -v -e 'rm $HOME/shut_down_*.log'

gpssh -f /home/gpadmin/shangz/hostfile_gemxd_servers -v -e 'rm -rf $HOME/gfxd'
gpssh -f /home/gpadmin/shangz/hostfile_gemxd_servers -v -e 'rm $HOME/gfxd_*.sh'

#!/bin/bash

CLIENTPORT=1527
PEER_DISCOVERY_PORT=10101
GFXD_DATADIR=$HOME/gfxd
PATH=$PATH:/usr/local/gfxd/bin/

echo -e "\033[1mStarting GemFire XD server service\033[0m"
echo -e "\033[1m-------------------------------------------\033[0m"

if [ ! -d  $GFXD_DATADIR ]; then
    mkdir $GFXD_DATADIR
fi

if [ ! -d $GFXD_DATADIR/server.$HOSTNAME ]; then
	mkdir -p $GFXD_DATADIR/server.$HOSTNAME
fi

gfxd server start -dir=$GFXD_DATADIR/server.$HOSTNAME -locators=hdw8.gphd.local[$PEER_DISCOVERY_PORT] -bind-address=$HOSTNAME -client-port=$CLIENTPORT
#!/bin/bash

CLIENTPORT=1527
PEER_DISCOVERY_PORT=10101
GFXD_DATADIR=$HOME/gfxd
PATH=$PATH:/usr/local/gfxd/bin/

echo -e "\033[1mStarting GemFire XD locator service\033[0m"
echo -e "\033[1m-------------------------------------------\033[0m"

if [ ! -d  $GFXD_DATADIR ]; then
    mkdir $GFXD_DATADIR
fi

if [ ! -d $GFXD_DATADIR/locator.$HOSTNAME ]; then
	mkdir -p $GFXD_DATADIR/locator.$HOSTNAME
fi

gfxd locator start -dir=$GFXD_DATADIR/locator.$HOSTNAME -peer-discovery-port=$PEER_DISCOVERY_PORT -peer-discovery-address=$HOSTNAME -client-port=$CLIENTPORT -jmx-manager=false -heap-size=10240m
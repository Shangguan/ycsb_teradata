package com.yahoo.ycsb.db;

/**
 * Constnats used by Teradata Client
 *
 * @author shangz
 */

public interface TeradataClientConstants {

    public static final String DRIVER_CLASS = "db.driver";

    public static final String CONNECTION_URL = "db.url";

    public static final String CONNECTION_USER = "db.user";

    public static final String CONNECTION_PASSWD = "db.passwd";

    public static final String FETCH_SIZE = "jdbc.fetchsize";

    public static final String AUTO_COMMIT = "jdbc.autocommit";

    public static final String FIELD_COUNT_PROPERTY = "fieldcount";

    public static final String FIELD_COUNT_PROPERTY_DEFAULT = "10";

    public static final String NULL_VALUE = "NULL";

    public static String PRIMARY_KEY = "YCSB_KEY";

    public static String COLUMN_PREFIX = "FIELD";

    public static final int SUCCESS = 0;

}

package com.yahoo.ycsb.db;

import com.yahoo.ycsb.ByteIterator;
import com.yahoo.ycsb.DB;
import com.yahoo.ycsb.DBException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class TeradataClient extends DB implements TeradataClientConstants {

    private Connection conn;
    private boolean initialized = false;
    private Properties props;
    private Integer fetchSize;
    private static final String DEFAULT_PROP = "";
    private ConcurrentMap<StatementType, PreparedStatement> cachedStatements;

    /**
     * The statement type for the prepared statements.
     */
    private static class StatementType {

        enum Type {
            INSERT(1),
            DELETE(2),
            READ(3),
            UPDATE(4),
            SCAN(5),;
            int internalType;

            private Type(int type) {
                internalType = type;
            }

            int getHashCode() {
                final int prime = 31;
                int result = 1;
                result = prime * result + internalType;
                return result;
            }
        }

        Type type;
        int numFields;
        String tableName;

        StatementType(Type type, String tableName, int numFields) {
            this.type = type;
            this.tableName = tableName;
            this.numFields = numFields;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + numFields;
            result = prime * result
                    + ((tableName == null) ? 0 : tableName.hashCode());
            result = prime * result + ((type == null) ? 0 : type.getHashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            StatementType other = (StatementType) obj;
            if (numFields != other.numFields)
                return false;
            if (tableName == null) {
                if (other.tableName != null)
                    return false;
            } else if (!tableName.equals(other.tableName))
                return false;
            if (type != other.type)
                return false;
            return true;
        }
    }

    private PreparedStatement createAndCacheInsertStatement(StatementType insertType, String key)
            throws SQLException {
        StringBuilder insert = new StringBuilder("INSERT INTO ");
        insert.append(insertType.tableName);
        insert.append(" VALUES(?");
        for (int i = 0; i < insertType.numFields; i++) {
            insert.append(",?");
        }
        insert.append(");");
        PreparedStatement insertStatement = conn.prepareStatement(insert.toString());
        PreparedStatement stmt = cachedStatements.putIfAbsent(insertType, insertStatement);
        if (stmt == null) return insertStatement;
        else return stmt;
    }

    private PreparedStatement createAndCacheReadStatement(StatementType readType, String key)
            throws SQLException {
        StringBuilder read = new StringBuilder("SELECT * FROM ");
        read.append(readType.tableName);
        read.append(" WHERE ");
        read.append(PRIMARY_KEY);
        read.append(" = ");
        read.append("?;");
        PreparedStatement readStatement = conn.prepareStatement(read.toString());
        PreparedStatement stmt = cachedStatements.putIfAbsent(readType, readStatement);
        if (stmt == null) return readStatement;
        else return stmt;
    }

    private PreparedStatement createAndCacheDeleteStatement(StatementType deleteType, String key)
            throws SQLException {
        StringBuilder delete = new StringBuilder("DELETE FROM ");
        delete.append(deleteType.tableName);
        delete.append(" WHERE ");
        delete.append(PRIMARY_KEY);
        delete.append(" = ?;");
        PreparedStatement deleteStatement = conn.prepareStatement(delete.toString());
        PreparedStatement stmt = cachedStatements.putIfAbsent(deleteType, deleteStatement);
        if (stmt == null) return deleteStatement;
        else return stmt;
    }

    private PreparedStatement createAndCacheUpdateStatement(StatementType updateType, String key)
            throws SQLException {
        StringBuilder update = new StringBuilder("UPDATE ");
        update.append(updateType.tableName);
        update.append(" SET ");
        for (int i = 1; i <= updateType.numFields; i++) {
            update.append(COLUMN_PREFIX);
            update.append(i);
            update.append("=?");
            if (i < updateType.numFields) update.append(", ");
        }
        update.append(" WHERE ");
        update.append(PRIMARY_KEY);
        update.append(" = ?;");
        PreparedStatement insertStatement = conn.prepareStatement(update.toString());
        PreparedStatement stmt = cachedStatements.putIfAbsent(updateType, insertStatement);
        if (stmt == null) return insertStatement;
        else return stmt;
    }

    private PreparedStatement createAndCacheScanStatement(StatementType scanType, String key)
            throws SQLException {
        StringBuilder select = new StringBuilder("SELECT * FROM ");
        select.append(scanType.tableName);
        select.append(" WHERE ");
        select.append(PRIMARY_KEY);
        select.append(" >= ");
        select.append("?;");
        PreparedStatement scanStatement = conn.prepareStatement(select.toString());
        if (this.fetchSize != null) scanStatement.setFetchSize(this.fetchSize);
        PreparedStatement stmt = cachedStatements.putIfAbsent(scanType, scanStatement);
        if (stmt == null) return scanStatement;
        else return stmt;
    }

    /**
     * Initialize the database connection and set it up for sending requests to the database.
     * This must be called once per client.
     *
     * @throws
     */
    @Override
    public void init() throws DBException {
        if (initialized) {
            System.err.println("Client connection already initialized.");
            return;
        }

        props = getProperties();
        String url = props.getProperty(CONNECTION_URL, DEFAULT_PROP);
        String user = props.getProperty(CONNECTION_USER, DEFAULT_PROP);
        String password = props.getProperty(CONNECTION_PASSWD, DEFAULT_PROP);
        String driver = props.getProperty(DRIVER_CLASS);

        String fetchSizeStr = props.getProperty(FETCH_SIZE);
        if (fetchSizeStr != null) {
            try {
                this.fetchSize = Integer.parseInt(fetchSizeStr);
            } catch (NumberFormatException nfe) {
                System.err.println("Invalid fetch size specified: " + fetchSizeStr);
                throw new DBException(nfe);
            }
        }

        String autoCommitStr = props.getProperty(AUTO_COMMIT, Boolean.TRUE.toString());
        Boolean autoCommit = Boolean.parseBoolean(autoCommitStr);

        try {
            if (driver != null) {
                Class.forName(driver);
            }

            conn = DriverManager.getConnection(url, user, password);
            conn.setAutoCommit(autoCommit);
            cachedStatements = new ConcurrentHashMap<StatementType, PreparedStatement>();
        } catch (ClassNotFoundException e) {
            System.err.println("Error in initializing the Teradata JDBC driver: " + e);
            throw new DBException(e);
        } catch (SQLException e) {
            System.err.println("Error in database operation: " + e);
            throw new DBException(e);
        } catch (NumberFormatException e) {
            System.err.println("Invalid value for fieldcount property. " + e);
            throw new DBException(e);
        }
        initialized = true;
    }

    @Override
    public int read(String table, String key, Set<String> fields, HashMap<String, ByteIterator> result) {
        return 0;
    }

    @Override
    public int scan(String table, String startkey, int recordcount, Set<String> fields, Vector<HashMap<String, ByteIterator>> result) {
        return 0;
    }

    @Override
    public int update(String table, String key, HashMap<String, ByteIterator> values) {
        return 0;
    }

    @Override
    public int insert(String tableName, String key, HashMap<String, ByteIterator> values) {
        if (tableName == null) {
            return -1;
        }
        if (key == null) {
            return -1;
        }

        try {
            int numFields = values.size();
            StatementType type = new StatementType(StatementType.Type.INSERT, tableName, numFields);
            PreparedStatement insertStatement = cachedStatements.get(type);
            if (insertStatement == null) {
                insertStatement = createAndCacheInsertStatement(type, key);
            }
            insertStatement.setString(1, key);
            int index = 2;
            for (Map.Entry<String, ByteIterator> entry : values.entrySet()) {
                String field = entry.getValue().toString();
                insertStatement.setString(index++, field);
            }
            int result = insertStatement.executeUpdate();
            if (result == 1) return SUCCESS;
            else return 1;
        } catch (SQLException e) {
            System.err.println("Error in processing insert to table: " + tableName + e);
            return -1;
        }
    }

    @Override
    public int delete(String table, String key) {
        return 0;
    }

    @Override
    public void cleanup() throws DBException {
        try {
            conn.close();
        } catch (SQLException e) {
            System.err.println("Error in closing the connection. " + e);
            throw new DBException(e);
        }
    }
}
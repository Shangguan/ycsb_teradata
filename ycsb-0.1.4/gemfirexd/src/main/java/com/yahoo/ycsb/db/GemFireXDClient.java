/**
 * Copyright (c) 2013 GoPivotal Inc. All rights reserved.
 */

package com.yahoo.ycsb.db;


import com.pivotal.gemfirexd.FabricServer;
import com.pivotal.gemfirexd.FabricServiceManager;

import com.yahoo.ycsb.DB;
import com.yahoo.ycsb.DBException;
import com.yahoo.ycsb.ByteIterator;
import com.yahoo.ycsb.LongByteIterator;
import com.yahoo.ycsb.StringByteIterator;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * A class that wraps a JDBC compliant database to allow it to be interfaced with YCSB.
 * This class extends {@link DB} and implements the database interface used by YCSB client.
 * <p/>
 * <br> Each client will have its own instance of this class. This client is
 * not thread safe.
 * <p/>
 * <br> This interface expects a schema <key> <field1> <field2> <field3> ...
 * All attributes are of type VARCHAR. All accesses are through the primary key. Therefore,
 * only one index on the primary key is needed.
 * <p/>
 * <p> The following options must be passed when using this database client.
 * <p/>
 * <ul>
 * <li><b>db.driver</b> The JDBC driver class to use.</li>
 * <li><b>db.url</b> The Database connection URL.</li>
 * </ul>
 */
public class GemFireXDClient extends DB implements GemFireXDConstants {

    private static final String FIELD2 = COLUMN_PREFIX + 2;
    private static final String FIELD3 = COLUMN_PREFIX + 3;

    private Connection conn;
    private boolean initialized = false;
    private Properties props;
    private ConcurrentMap<StatementType, PreparedStatement> cachedStatements;
    private long myScans = 0; // @lises allows counting scans
    private long myScanResults = 0; // @lises allows counting scan results

    /**
     * The statement type for the prepared statements.
     */
    private static class StatementType {

        enum Type {
            INSERT(1),
            DELETE(2),
            READ(3),
            UPDATE(4),
            SCAN(5),
            QUERY_WITH_FILTER(6),
            QUERY_WITH_AGGREGATE(7),
            QUERY_WITH_JOIN(8);
            int internalType;

            private Type(int type) {
                internalType = type;
            }

            int getHashCode() {
                final int prime = 31;
                int result = 1;
                result = prime * result + internalType;
                return result;
            }
        }

        Type type;
        int numFields;
        String tableName;
        String table2;

        StatementType(Type type, String tableName, int numFields) {
            this.type = type;
            this.tableName = tableName;
            this.numFields = numFields;
        }

        StatementType(Type type, String tableName, String table2) {
            this.type = type;
            this.tableName = tableName;
            this.table2 = table2;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + numFields;
            result = prime * result
                    + ((tableName == null) ? 0 : tableName.hashCode());
            result = prime * result + ((type == null) ? 0 : type.getHashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            StatementType other = (StatementType) obj;
            if (type != other.type) {
                return false;
            }
            if (numFields != other.numFields) {
                return false;
            }
            if (tableName == null) {
                if (other.tableName != null)
                    return false;
            } else if (!tableName.equals(other.tableName)) {
                return false;
            }
            return true;
        }
    }

    /**
     * Initialize the database connection and set it up for sending requests to the database.
     * This must be called once per client.
     *
     * @throws DBException
     */
    @Override
    public void init() throws DBException {
        if (initialized) {
            System.err.println("Client connection already initialized.");
            return;
        }
        props = getProperties();
        String url = props.getProperty(CONNECTION_URL);
        String driver = props.getProperty(DRIVER_CLASS);

        try {
            if (driver != null) {
                Class.forName(driver);
            }
            Properties connProps;
            assert driver != null;
            if (driver.contains("EmbeddedDriver")) {
                conn = getPeerClientConnection(url);
            } else {
                connProps = getConnectionProperties();
                System.out.println("\nThin client connecting to " + url + " with properties: " + connProps);
                conn = DriverManager.getConnection(url, connProps);
            }
            conn.setAutoCommit(false);
            conn.setTransactionIsolation(Connection.TRANSACTION_NONE);

            cachedStatements = new ConcurrentHashMap<StatementType, PreparedStatement>();
        } catch (ClassNotFoundException e) {
            System.err.println("Error in initializing the JDBC driver: " + e);
            throw new DBException(e);
        } catch (SQLException e) {
            System.err.println("Error in database operation: " + e);
            throw new DBException(e);
        }
        initialized = true;
    }

    @Override
    public void cleanup() throws DBException {
        System.out.println("\nCompleted " + myScans + " scans with " + myScanResults + " results"); // @lises report scans
        try {
            conn.close();
        } catch (SQLException e) {
            System.err.println("Error in closing the connection. " + e);
            throw new DBException(e);
        }
    }

    private PreparedStatement createAndCacheInsertStatement(StatementType insertType, String key)
            throws SQLException {
        StringBuilder insert = new StringBuilder("PUT INTO ");
        insert.append(insertType.tableName);
        insert.append(" VALUES(?");
        for (int i = 0; i < insertType.numFields; i++) {
            insert.append(",?");
        }
        insert.append(");");
        PreparedStatement insertStatement = conn.prepareStatement(insert.toString());
        PreparedStatement stmt = cachedStatements.putIfAbsent(insertType, insertStatement);
        if (stmt == null) return insertStatement;
        else return stmt;
    }

    private PreparedStatement createAndCacheReadStatement(StatementType readType, String key)
            throws SQLException {
        StringBuilder read = new StringBuilder("SELECT * FROM ");
        read.append(readType.tableName);
        String queryHDFS = props.getProperty(QUERY_HDFS);
        if (queryHDFS.equals("true")) {
            read.append(" --GEMFIREXD-PROPERTIES queryHDFS=true \n");
        }
        read.append(" WHERE ");
        read.append(PRIMARY_KEY);
        read.append(" = ");
        read.append("?;");
        PreparedStatement readStatement = conn.prepareStatement(read.toString());
        PreparedStatement stmt = cachedStatements.putIfAbsent(readType, readStatement);
        if (stmt == null) return readStatement;
        else return stmt;
    }

    private PreparedStatement createAndCacheDeleteStatement(StatementType deleteType, String key)
            throws SQLException {
        StringBuilder delete = new StringBuilder("DELETE FROM ");
        delete.append(deleteType.tableName);
        delete.append(" WHERE ");
        delete.append(PRIMARY_KEY);
        delete.append(" = ?;");
        PreparedStatement deleteStatement = conn.prepareStatement(delete.toString());
        PreparedStatement stmt = cachedStatements.putIfAbsent(deleteType, deleteStatement);
        if (stmt == null) return deleteStatement;
        else return stmt;
    }

    private PreparedStatement createAndCacheUpdateStatement(StatementType updateType, String key, Set<String> fields)
            throws SQLException {
        StringBuilder update = new StringBuilder("UPDATE ");
        update.append(updateType.tableName);
        update.append(" SET ");
        int i = 1;
        for (String field : fields) {
            update.append(field);
            update.append("=?");
            if (i < updateType.numFields) update.append(", ");
            ++i;
        }
        update.append(" WHERE ");
        update.append(PRIMARY_KEY);
        update.append(" = ?;");
        PreparedStatement updateStatement = conn.prepareStatement(update.toString());
        PreparedStatement stmt = cachedStatements.putIfAbsent(updateType, updateStatement);
        if (stmt == null) return updateStatement;
        else return stmt;
    }

    private PreparedStatement createAndCacheScanStatement(StatementType scanType, String key)
            throws SQLException {
        StringBuilder select = new StringBuilder("SELECT * FROM ");
        select.append(scanType.tableName);
        String queryHDFS = props.getProperty(QUERY_HDFS);
        if (queryHDFS.equals("true")) {
            select.append(" --GEMFIREXD-PROPERTIES queryHDFS=true \n");
        }
        select.append(" WHERE ");
        select.append(PRIMARY_KEY);
        select.append(" >= ");
        select.append("?;");
        PreparedStatement scanStatement = conn.prepareStatement(select.toString());
        PreparedStatement stmt = cachedStatements.putIfAbsent(scanType, scanStatement);
        if (stmt == null) return scanStatement;
        else return stmt;
    }

    private PreparedStatement createAndCacheQueryWithFilterStatement(StatementType query)
            throws SQLException {
        StringBuilder select = new StringBuilder("SELECT * FROM ");
        select.append(query.tableName);
        String queryHDFS = props.getProperty(QUERY_HDFS);
        if (queryHDFS.equals("true")) {
            select.append(" u --GEMFIREXD-PROPERTIES queryHDFS=true \n");
        } else {
            select.append(" u");
        }
        select.append(" WHERE u.field0 = ? AND u.field2 > ? AND u.field2 < ?");
        select.append(" FETCH FIRST ? ROWS ONLY;");
        PreparedStatement queryStatement = conn.prepareStatement(select.toString());
        PreparedStatement stmt = cachedStatements.putIfAbsent(query, queryStatement);
        if (stmt == null) return queryStatement;
        else return stmt;
    }

    private PreparedStatement createAndCacheQueryWithAggregateStatement(StatementType query)
            throws SQLException {
        StringBuilder select = new StringBuilder("SELECT u.field1, sum(u.field3) FROM ");
        select.append(query.tableName);
        String queryHDFS = props.getProperty(QUERY_HDFS);
        if (queryHDFS.equals("true")) {
            select.append(" u --GEMFIREXD-PROPERTIES queryHDFS=true \n");
        } else {
            select.append(" u");
        }
        select.append(" WHERE u.field0 = ? AND u.field2 > ? AND u.field2 < ?");
        select.append(" GROUP BY u.field1 ORDER BY u.field1");
        select.append(" FETCH FIRST ? ROWS ONLY;");
        PreparedStatement queryStatement = conn.prepareStatement(select.toString());
        PreparedStatement stmt = cachedStatements.putIfAbsent(query, queryStatement);
        if (stmt == null) return queryStatement;
        else return stmt;
    }

    private PreparedStatement createAndCacheQueryWithJoinStatement(StatementType query)
            throws SQLException {
        StringBuilder select = new StringBuilder("SELECT u.ycsb_key, v.ycsb_key, u.field1, v.field1 FROM ");
        select.append(query.tableName);
        select.append(" u JOIN ");
        select.append(query.table2);
	String queryHDFS = props.getProperty(QUERY_HDFS);
        if (queryHDFS.equals("true")) {
            select.append(" v --GEMFIREXD-PROPERTIES queryHDFS=true \n");
        } else {
            select.append(" v");
	}
        select.append(" ON u.ycsb_key = v.ycsb_key");
        select.append(" WHERE u.field0 = ? AND u.field2 > ? AND u.field2 < ?");
        select.append(" ORDER BY u.field1, v.field1");
        select.append(" FETCH FIRST ? ROWS ONLY;");
        PreparedStatement queryStatement = conn.prepareStatement(select.toString());
        PreparedStatement stmt = cachedStatements.putIfAbsent(query, queryStatement);
        if (stmt == null) return queryStatement;
        else return stmt;
    }

    @Override
    public int read(String tableName, String key, Set<String> fields,
                    HashMap<String, ByteIterator> result) {
        if (tableName == null) {
            return -1;
        }
        if (key == null) {
            return -1;
        }
        try {
            StatementType type = new StatementType(StatementType.Type.READ, tableName, 1);
            PreparedStatement readStatement = cachedStatements.get(type);
            if (readStatement == null) {
                readStatement = createAndCacheReadStatement(type, key);
            }
            readStatement.setString(1, key);
            ResultSet resultSet = readStatement.executeQuery();
            if (!resultSet.next()) {
                resultSet.close();
                return -1;
            }
            if (result != null && fields != null) {
                for (String field : fields) {
                    String value = resultSet.getString(field);
                    result.put(field, new StringByteIterator(value));
                }
            }
            resultSet.close();
            return SUCCESS;
        } catch (SQLException e) {
            System.err.println("Error in processing read of table " + tableName + ": " + e);
            return -2;
        }
    }

    @Override
    public int scan(String tableName, String startKey, int recordcount,
                    Set<String> fields, Vector<HashMap<String, ByteIterator>> result) {
        if (tableName == null) {
            return -1;
        }
        if (startKey == null) {
            return -1;
        }
        try {
            StatementType type = new StatementType(StatementType.Type.SCAN, tableName, 1);
            PreparedStatement scanStatement = cachedStatements.get(type);
            if (scanStatement == null) {
                scanStatement = createAndCacheScanStatement(type, startKey);
            }
            scanStatement.setString(1, startKey);
            ResultSet resultSet = scanStatement.executeQuery();
            for (int i = 0; i < recordcount && resultSet.next(); i++) {
                ++myScanResults; // @lises count scan results
                if (result != null && fields != null) {
                    HashMap<String, ByteIterator> values = new HashMap<String, ByteIterator>();
                    for (String field : fields) {
                        String value = resultSet.getString(field);
                        values.put(field, new StringByteIterator(value));
                    }
                    result.add(values);
                }
            }
            ++myScans; // @lises count scans
            resultSet.close();
            return SUCCESS;
        } catch (SQLException e) {
            System.err.println("Error in processing scan of table: " + tableName + e);
            return -2;
        }
    }

    @Override
    public int update(String tableName, String key, HashMap<String, ByteIterator> values) {
        if (tableName == null) {
            return -1;
        }
        if (key == null) {
            return -1;
        }
        try {
            int numFields = values.size();
            StatementType type = new StatementType(StatementType.Type.UPDATE, tableName, numFields);
            PreparedStatement updateStatement = cachedStatements.get(type);
            if (updateStatement == null) {
                updateStatement = createAndCacheUpdateStatement(type, key, values.keySet());
            }
            int i = 1;
            for (String field : values.keySet()) {
                ByteIterator bytes = values.get(field);
                if (field.equals(FIELD2) || field.equals(FIELD3)) {
                    updateStatement.setLong(i, ((LongByteIterator) bytes).toLong());
                } else {
                    updateStatement.setString(i, bytes.toString());
                }
                ++i;
            }
            updateStatement.setString(i, key);
            int result = updateStatement.executeUpdate();
            if (result == 1) return SUCCESS;
            else return 1;
        } catch (SQLException e) {
            System.err.println("Error in processing update to table: " + tableName + e);
            return -1;
        }
    }

    @Override
    public int insert(String tableName, String key, HashMap<String, ByteIterator> values) {
        if (tableName == null) {
            return -1;
        }
        if (key == null) {
            return -1;
        }
        try {
            int numFields = values.size();
            StatementType type = new StatementType(StatementType.Type.INSERT, tableName, numFields);
            PreparedStatement insertStatement = cachedStatements.get(type);
            if (insertStatement == null) {
                insertStatement = createAndCacheInsertStatement(type, key);
            }
            insertStatement.setString(1, key);
            for (int i = 0; i < numFields; i++) {
                ByteIterator bytes = values.get(COLUMN_PREFIX + i);
                if (i == 2 || i == 3) {
                    insertStatement.setLong(i + 2, ((LongByteIterator) bytes).toLong());
                } else {
                    insertStatement.setString(i + 2, bytes.toString());
                }
            }
            int result = insertStatement.executeUpdate();
            if (result == 1) return SUCCESS;
            else return -1;
        } catch (SQLException e) {
            System.err.println("Error in processing insert to table " + tableName + ": " + e);
            return -1;
        }
    }

    @Override
    public int delete(String tableName, String key) {
        if (tableName == null) {
            return -1;
        }
        if (key == null) {
            return -1;
        }
        try {
            StatementType type = new StatementType(StatementType.Type.DELETE, tableName, 1);
            PreparedStatement deleteStatement = cachedStatements.get(type);
            if (deleteStatement == null) {
                deleteStatement = createAndCacheDeleteStatement(type, key);
            }
            deleteStatement.setString(1, key);
            int result = deleteStatement.executeUpdate();
            if (result == 1) return SUCCESS;
            else return 1;
        } catch (SQLException e) {
            System.err.println("Error in processing delete to table: " + tableName + e);
            return -1;
        }
    }

    public int queryWithFilter(String table, String field0, String field2,
                               String field0value, long field2start, long field2end, int recordcount,
                               Vector<HashMap<String, ByteIterator>> result) {
        if (table == null) {
            return -1;
        }
        if (field0value == null) {
            return -1;
        }
        try {
            StatementType type = new StatementType(StatementType.Type.QUERY_WITH_FILTER, table, 1);
            PreparedStatement queryStatement = cachedStatements.get(type);
            if (queryStatement == null) {
                queryStatement = createAndCacheQueryWithFilterStatement(type);
            }

            queryStatement.setString(1, field0value);
            queryStatement.setLong(2, field2start);
            queryStatement.setLong(3, field2end);
            queryStatement.setInt(4, recordcount);

            ResultSet resultSet = queryStatement.executeQuery();
            resultSet.setFetchSize(recordcount);

            int resultCount = 0;
            for (int i = 0; i < recordcount && resultSet.next(); i++) {
                if (result != null) {
                    HashMap<String, ByteIterator> values = new HashMap<String, ByteIterator>();

                    // loop is unrolled
                    values.put(GemFireXDConstants.COLUMN_PREFIX + "0", new StringByteIterator(resultSet.getString(2)));
                    values.put(GemFireXDConstants.COLUMN_PREFIX + "1", new StringByteIterator(resultSet.getString(3)));
                    values.put(GemFireXDConstants.COLUMN_PREFIX + "2", new LongByteIterator(resultSet.getLong(4)));
                    values.put(GemFireXDConstants.COLUMN_PREFIX + "3", new LongByteIterator(resultSet.getLong(5)));
                    values.put(GemFireXDConstants.COLUMN_PREFIX + "4", new StringByteIterator(resultSet.getString(6)));
                    values.put(GemFireXDConstants.COLUMN_PREFIX + "5", new StringByteIterator(resultSet.getString(7)));
                    values.put(GemFireXDConstants.COLUMN_PREFIX + "6", new StringByteIterator(resultSet.getString(8)));
                    values.put(GemFireXDConstants.COLUMN_PREFIX + "7", new StringByteIterator(resultSet.getString(9)));
                    values.put(GemFireXDConstants.COLUMN_PREFIX + "8", new StringByteIterator(resultSet.getString(10)));
                    //TODO isn't this needed? values.put(GemFireXDConstants.COLUMN_PREFIX + "9", new StringByteIterator(resultSet.getString(11)));

                    result.add(values);
                    resultCount++;
                }
            }
            resultSet.close();
            return SUCCESS;
        } catch (SQLException e) {
            System.err.println("Error in processing query of table: " + table + e);
            return -2;
        }
    }

    public int queryWithAggregate(String table, String field0, String field1,
                                  String field2, String field3, String field0value, long field2start,
                                  long field2end, int recordcount, Vector<HashMap<String, ByteIterator>> result) {
        if (table == null) {
            return -1;
        }
        if (field0value == null) {
            return -1;
        }
        try {
            StatementType type = new StatementType(StatementType.Type.QUERY_WITH_AGGREGATE, table, 1);
            PreparedStatement queryStatement = cachedStatements.get(type);
            if (queryStatement == null) {
                queryStatement = createAndCacheQueryWithAggregateStatement(type);
            }

            queryStatement.setString(1, field0value);
            queryStatement.setLong(2, field2start);
            queryStatement.setLong(3, field2end);
            queryStatement.setInt(4, recordcount);

            ResultSet resultSet = queryStatement.executeQuery();
            resultSet.setFetchSize(recordcount);

            int resultCount = 0;
            for (int i = 0; i < recordcount && resultSet.next(); i++) {
                if (result != null) {
                    HashMap<String, ByteIterator> values = new HashMap<String, ByteIterator>();

                    values.put(GemFireXDConstants.COLUMN_PREFIX + "1", new StringByteIterator(resultSet.getString(1)));
                    values.put(GemFireXDConstants.COLUMN_PREFIX + "3", new LongByteIterator(resultSet.getLong(2)));

                    result.add(values);
                    resultCount++;
                }
            }
            resultSet.close();
            return SUCCESS;
        } catch (SQLException e) {
            System.err.println("Error in processing query of table: " + table + e);
            return -2;
        }
    }

    @Override
    public int queryWithJoin(String table, String table2, String field0value,
                             long field2start, long field2end, int recordcount,
                             Vector<HashMap<String, ByteIterator>> result) {
        if (table == null) {
            return -1;
        }
        if (field0value == null) {
            return -1;
        }
        try {
            StatementType type = new StatementType(StatementType.Type.QUERY_WITH_JOIN, table, table2);
            PreparedStatement queryStatement = cachedStatements.get(type);
            if (queryStatement == null) {
                queryStatement = createAndCacheQueryWithJoinStatement(type);
            }

            queryStatement.setString(1, field0value);
            queryStatement.setLong(2, field2start);
            queryStatement.setLong(3, field2end);
            queryStatement.setInt(4, recordcount);

            ResultSet resultSet = queryStatement.executeQuery();
            resultSet.setFetchSize(recordcount);

            int resultCount = 0;
            for (int i = 0; i < recordcount && resultSet.next(); i++) {
                if (result != null) {
                    HashMap<String, ByteIterator> values = new HashMap<String, ByteIterator>();

                    values.put(GemFireXDConstants.COLUMN_PREFIX + "0", new StringByteIterator(resultSet.getString(1)));
                    values.put(GemFireXDConstants.COLUMN_PREFIX + "1", new StringByteIterator(resultSet.getString(2)));
                    values.put(GemFireXDConstants.COLUMN_PREFIX + "2", new StringByteIterator(resultSet.getString(3)));
                    values.put(GemFireXDConstants.COLUMN_PREFIX + "3", new StringByteIterator(resultSet.getString(4)));

                    result.add(values);
                    resultCount++;
                }
            }
            resultSet.close();
            return SUCCESS;
        } catch (SQLException e) {
            System.err.println("Error in processing query of table: " + table + e);
            return -2;
        }
    }

    protected Properties getConnectionProperties() {
        Properties p = new Properties();
        p.setProperty("single-hop-enabled", "true");
        p.setProperty("query-HDFS", "false");
        return p;
    }

    private static boolean connectedAlready = false;

    protected static synchronized Connection getPeerClientConnection(String url) throws SQLException {
        Connection tmpconn = null;
        if (connectedAlready) {
            String peerurl = "jdbc:gemfirexd:";
            System.out.println("\nPeer client connecting to " + peerurl);
            tmpconn = DriverManager.getConnection(peerurl);
        } else {
            System.out.println("\nPeer client connecting to " + url);
            tmpconn = DriverManager.getConnection(url);
            sleepForMs(10000);
            connectedAlready = true;

        }
        return tmpconn;
    }

    private static void sleepForMs(int msToSleep) {
        if (msToSleep != 0) {
            try {
                Thread.sleep(msToSleep);
            } catch (InterruptedException ex) {
            }
        }
    }
}

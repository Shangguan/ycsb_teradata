/**
 * Copyright (c) 2013 GoPivotal Inc. All rights reserved.
 */
package com.yahoo.ycsb.db;

/**
 * Constants used by the JDBC client.
 */
public interface GemFireXDConstants {

  /** The class to use as the jdbc driver. */
  public static final String DRIVER_CLASS = "db.driver";
  
  /** The URL to connect to the database. */
  public static final String CONNECTION_URL = "db.url";
 
  /** The URL to connect to the name node. */
  public static final String HDFS_NAME_NODE_URL = "hdfs.name.node.url";

  /** home directory prefix in HDFS */
  public static final String HOME_DIR_PREFIX = "home.dir.prefix";
 
  /** Whether to use offheap memory. */
  public static final String OFFHEAP = "offheap";
 
  /** Query HDFS. */
  public static final String QUERY_HDFS = "query.hdfs";
 
  /** HDFS evict incoming for the table. */
  public static final String EVICT_INCOMING = "evict.incoming";
 
  /** HDFS async event queue max memory. */
  public static final String HDFS_MAX_QUEUE_MEMORY = "hdfs.max.queue.memory";
 
  /** HDFS async event queue persistence. */
  public static final String HDFS_QUEUE_PERSISTENT = "hdfs.queue.persistent";
 
  /** HDFS async event queue batch size. */
  public static final String HDFS_BATCH_SIZE = "hdfs.batch.size";
 
  /** HDFS minor compaction. */
  public static final String HDFS_MINOR_COMPACT = "hdfs.minor.compact";
 
  /** HDFS minor compaction threads. */
  public static final String HDFS_MINOR_COMPACTION_THREADS = "hdfs.minor.compaction.threads";
 
  /** HDFS client configuration file path. */
  public static final String HDFS_CLIENT_CONFIG_FILE = "hdfs.client.config.file";
 
  /** Partitioned table number of buckets. */
  public static final String PR_BUCKETS = "pr.buckets";
 
  /** Partitioned table redundancy. */
  public static final String PR_REDUNDANCY = "pr.redundancy";
 
  /** The name of the property for the number of fields in a record. */
  public static final String FIELD_COUNT_PROPERTY="fieldcount";
  
  /** Default number of fields in a record. */
  public static final String FIELD_COUNT_PROPERTY_DEFAULT="10";
  
  /** Representing a NULL value. */
  public static final String NULL_VALUE = "NULL";
  
  /** The code to return when the call succeeds. */
  public static final int SUCCESS = 0;
  
  /** The primary key in the user table.*/
  public static String PRIMARY_KEY = "YCSB_KEY";
  
  /** The field name prefix in the table.*/
  public static String COLUMN_PREFIX = "field";

  public static String GENERATE_QUERY_DATA = "generate.query.data";
  public static String GENERATE_TWO_TABLES = "generate.two.tables";
}

/**
 * Copyright (c) 2013 GoPivotal Inc. All rights reserved.
 */
package com.yahoo.ycsb.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Properties;

/**
 * Utility class to create the table to be used by the benchmark.
 */
public class GemFireXDCreateTable implements GemFireXDConstants {

    public static final String TABLENAME_PROPERTY = "table";
    public static final String TABLENAME2_PROPERTY = "table2";

    public static final String TABLENAME_PROPERTY_DEFAULT = "usertable";
    public static final String TABLENAME2_PROPERTY_DEFAULT = "usertable2";

    private static final String HDFS_DISKSTORE_NAME = "hdfsStoreDisk";
    private static final String HDFS_DISKDIR_NAME = "hdfsStoreData";
    private static final String HDFS_STORE_NAME = "hdfsstore";

    private static final String HDFS_DISKSTORE_NAME2 = "hdfsStoreDisk2";
    private static final String HDFS_DISKDIR_NAME2 = "hdfsStoreData2";
    private static final String HDFS_STORE_NAME2 = "hdfsstore2";

    private static void usageMessage() {
        System.out.println("Create Table Client. Options:");
        System.out.println("  -p   key=value properties defined.");
        System.out.println("  -P   location of the properties file to load.");
        System.out.println("  -f   number of fields (default 10).");
    }

    private static Connection getConnection(Properties props)
            throws SQLException {
        String driver = props.getProperty(DRIVER_CLASS);
        String url = props.getProperty(CONNECTION_URL);
        if (driver == null || url == null) {
            throw new SQLException("Missing connection information.");
        }
        Connection conn = null;
        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url);
        } catch (ClassNotFoundException e) {
            throw new SQLException("JDBC Driver class not found.");
        }
        return conn;
    }

    private static void dropTable(Connection conn, String tableName) throws SQLException {
        StringBuilder sql = new StringBuilder("DROP TABLE IF EXISTS ");
        sql.append(tableName).append(";");
        execute(conn, sql.toString());
    }

    private static void dropHdfsStore(Connection conn, String hdfsStoreName) throws SQLException {
        StringBuilder sql = new StringBuilder("DROP HDFSSTORE IF EXISTS ");
        sql.append(hdfsStoreName).append(";");
        execute(conn, sql.toString());
    }

    private static void dropDiskStore(Connection conn, String diskStoreName) throws SQLException {
        StringBuilder sql = new StringBuilder("DROP DISKSTORE IF EXISTS ");
        sql.append(diskStoreName).append(";");
        execute(conn, sql.toString());
    }

    /**
     * Drop everything: tables, HDFS stores, disk stores.
     */
    private static void dropAll(Connection conn, String tablename, String tablename2)
            throws SQLException {
        if (tablename2 != null) {
            dropTable(conn, tablename2);
            dropHdfsStore(conn, HDFS_STORE_NAME2);
            dropDiskStore(conn, HDFS_DISKSTORE_NAME2);
        }
        dropTable(conn, tablename);
        dropHdfsStore(conn, HDFS_STORE_NAME);
        dropDiskStore(conn, HDFS_DISKSTORE_NAME);
    }

    private static void createTable(Connection conn, Properties props, String tableName, String hdfsStoreName, String colocatedTable) throws SQLException {
        int fieldcount = Integer.parseInt(props.getProperty(FIELD_COUNT_PROPERTY,
                FIELD_COUNT_PROPERTY_DEFAULT));
        if (fieldcount < 6) {
            throw new SQLException("Need at least 5 fields in " + FIELD_COUNT_PROPERTY);
        }

        String buckets = props.getProperty(PR_BUCKETS);
        String redundancy = props.getProperty(PR_REDUNDANCY);
        StringBuilder sql = new StringBuilder("CREATE TABLE ");
        sql.append(tableName)
                .append(" (")
                .append(PRIMARY_KEY).append(" VARCHAR(100) PRIMARY KEY");
        for (int idx = 0; idx < fieldcount; idx++) {
            sql.append(", FIELD");
            sql.append(idx);
            if (idx == 2 || idx == 3) {
                sql.append(" BIGINT");
            } else {
                sql.append(" VARCHAR(100)");
            }
        }

        if (colocatedTable != null) {
            sql.append(")")
                    .append(" partition by (")
                    .append(PRIMARY_KEY)
                    .append(")");
            sql.append(" colocate with (")
                    .append(colocatedTable)
                    .append(")");
        } else {
            sql.append(")")
                    .append(" partition by (")
                    .append(PRIMARY_KEY)
                    .append(")");
        }
        sql.append(" buckets ").append(buckets)
                .append(" redundancy ").append(redundancy);

        String nameNodeURL = props.getProperty(HDFS_NAME_NODE_URL);
        if (nameNodeURL != null) {
            String evictIncoming = props.getProperty(EVICT_INCOMING);
            if (evictIncoming.equals("true")) {
//                sql.append(" EVICTION BY CRITERIA (1=1 OR YCSB_KEY='') EVICT INCOMING");
                sql.append(" EVICTION BY CRITERIA (mod(FIELD2, 10)=1) EVICT INCOMING");
            }

            sql.append(" HDFSSTORE (").append(hdfsStoreName).append(")");
        }
        sql.append(";");
        execute(conn, sql.toString());

        // create index on primary key, FIELD 0 to 3, on FIELD 4 for second table
        sql = new StringBuilder("CREATE INDEX ");
        sql.append("pk_").append(tableName).append("_idx")
                .append(" ON ").append(tableName).append(" (YCSB_KEY);");
        execute(conn, sql.toString());
        sql = new StringBuilder("CREATE INDEX ");
        sql.append("field0_").append(tableName).append("_idx")
                .append(" ON ").append(tableName).append(" (FIELD0);");
        execute(conn, sql.toString());
        sql = new StringBuilder("CREATE INDEX ");
        sql.append("field1_").append(tableName).append("_idx")
                .append(" ON ").append(tableName).append(" (FIELD1);");
        execute(conn, sql.toString());
        sql = new StringBuilder("CREATE INDEX ");
        sql.append("field2_").append(tableName).append("_idx")
                .append(" ON ").append(tableName).append(" (FIELD2);");
        execute(conn, sql.toString());
        sql = new StringBuilder("CREATE INDEX ");
        sql.append("field3_").append(tableName).append("_idx")
                .append(" ON ").append(tableName).append(" (FIELD3);");
        execute(conn, sql.toString());
        if (colocatedTable != null) {
            sql = new StringBuilder("CREATE INDEX ");
            sql.append("field4_").append(tableName).append("_idx")
                    .append(" ON ").append(tableName).append(" (FIELD4);");
            execute(conn, sql.toString());
        }

        // create buckets
        createBuckets(conn, tableName);
    }

    /**
     * Create all buckets to ensure perfect balance.
     */
    private static void createBuckets(Connection conn, String tablename)
            throws SQLException {
        String procedure = "call SYS.CREATE_ALL_BUCKETS(?)";
        System.out.println(getDateTimeStamp() + "Executing CALL: " + procedure + " on " + tablename);
        CallableStatement call = conn.prepareCall(procedure);
        call.setString(1, tablename);
        call.execute();
        System.out.println(getDateTimeStamp() + "Executed CALL: " + procedure + " on " + tablename);
    }

    /**
     * Create hdfs store with disk store for hdfs async event queue.
     */
    private static void createStores(Connection conn, Properties props, String diskStoreName, String diskDir, String hdfsStoreName)
            throws SQLException {
        String nameNodeURL = props.getProperty(HDFS_NAME_NODE_URL);
        if (nameNodeURL != null) {
            String queuePersistent = props.getProperty(HDFS_QUEUE_PERSISTENT);
            if (queuePersistent.equalsIgnoreCase("true")) {
                //--- create disk store for hdfs async event queue
                StringBuilder sql = new StringBuilder("CREATE DISKSTORE ");
                sql.append(diskStoreName)
                        .append(" autocompact false ")
                        .append("('")
                        .append(diskDir)
                        .append("');");
                execute(conn, sql.toString());
            }
            String homeDir = props.getProperty(HOME_DIR_PREFIX);
            String maxQueueMemory = props.getProperty(HDFS_MAX_QUEUE_MEMORY);
            String batchSize = props.getProperty(HDFS_BATCH_SIZE);
            String minorCompact = props.getProperty(HDFS_MINOR_COMPACT);
            String minorCompactionThreads = props.getProperty(HDFS_MINOR_COMPACTION_THREADS);
            if (queuePersistent.equalsIgnoreCase("true")) {
                StringBuilder sql = new StringBuilder("CREATE HDFSSTORE ");
                sql.append(hdfsStoreName)
                        .append(" NAMENODE '").append(nameNodeURL).append("'")
                        .append(" HOMEDIR '/").append(homeDir + "_" + hdfsStoreName).append("'")
                        .append(" DISKSTORENAME ").append(diskStoreName)
                        .append(" MINORCOMPACT ").append(minorCompact)
                        .append(" MINORCOMPACTIONTHREADS ").append(minorCompactionThreads)
                        .append(" MAJORCOMPACT false")
                        .append(" QUEUEPERSISTENT ").append(String.valueOf(queuePersistent))
                        .append(" BATCHSIZE ").append(batchSize)
                        .append(" MAXQUEUEMEMORY ").append(maxQueueMemory);
                execute(conn, sql.toString());
            }
        }
    }

    private static void execute(Connection c, String sql)
            throws SQLException {
        System.out.println(getDateTimeStamp() + "Executing SQL: " + sql);
        Statement stmt = c.createStatement();
        stmt.execute(sql);
        System.out.println(getDateTimeStamp() + "Executed SQL: " + sql);
    }

    /**
     * @param args
     */
    public static void main(String[] args) throws SQLException {
        if (args.length == 0) {
            usageMessage();
            System.exit(0);
        }

        int fieldcount = -1;
        Properties props = new Properties();
        Properties fileprops = new Properties();
        Connection conn = null;

        // parse arguments
        int argindex = 0;
        while (args[argindex].startsWith("-")) {
            if (args[argindex].compareTo("-P") == 0) {
                argindex++;
                if (argindex >= args.length) {
                    usageMessage();
                    System.exit(0);
                }
                String propfile = args[argindex];
                argindex++;

                Properties myfileprops = new Properties();
                try {
                    myfileprops.load(new FileInputStream(propfile));
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                    System.exit(0);
                }

                // Issue #5 - remove call to stringPropertyNames to make compilable
                // under Java 1.5
                for (Enumeration<?> e = myfileprops.propertyNames(); e
                        .hasMoreElements(); ) {
                    String prop = (String) e.nextElement();

                    fileprops.setProperty(prop, myfileprops.getProperty(prop));
                }

            } else if (args[argindex].compareTo("-p") == 0) {
                argindex++;
                if (argindex >= args.length) {
                    usageMessage();
                    System.exit(0);
                }
                int eq = args[argindex].indexOf('=');
                if (eq < 0) {
                    usageMessage();
                    System.exit(0);
                }

                String name = args[argindex].substring(0, eq);
                String value = args[argindex].substring(eq + 1);
                props.put(name, value);
                argindex++;
            } else if (args[argindex].compareTo("-f") == 0) {
                argindex++;
                if (argindex >= args.length) {
                    usageMessage();
                    System.exit(0);
                }
                try {
                    fieldcount = Integer.parseInt(args[argindex++]);
                } catch (NumberFormatException e) {
                    System.err.println("Invalid number for field count");
                    usageMessage();
                    System.exit(1);
                }
            } else {
                System.out.println("Unknown option " + args[argindex]);
                usageMessage();
                System.exit(0);
            }

            if (argindex >= args.length) {
                break;
            }
        }

        if (argindex != args.length) {
            usageMessage();
            System.exit(0);
        }

        // overwrite file properties with properties from the command line

        // Issue #5 - remove call to stringPropertyNames to make compilable under
        // Java 1.5
        for (Enumeration<?> e = props.propertyNames(); e.hasMoreElements(); ) {
            String prop = (String) e.nextElement();

            fileprops.setProperty(prop, props.getProperty(prop));
        }

        props = fileprops;

        if (fieldcount > 0) {
            props.setProperty(FIELD_COUNT_PROPERTY, String.valueOf(fieldcount));
        }

        try {
            String tablename = props.getProperty(TABLENAME_PROPERTY, TABLENAME_PROPERTY_DEFAULT);
            String tablename2 = props.getProperty(TABLENAME2_PROPERTY, TABLENAME2_PROPERTY_DEFAULT);

            boolean generatequerydata = Boolean.parseBoolean(props.getProperty(GENERATE_QUERY_DATA));
            boolean generatetwotables = Boolean.parseBoolean(props.getProperty(GENERATE_TWO_TABLES));

            conn = getConnection(props);
            if (generatetwotables) {
                dropAll(conn, tablename, tablename2);
                createStores(conn, props, HDFS_DISKSTORE_NAME, HDFS_DISKDIR_NAME, HDFS_STORE_NAME);
                createStores(conn, props, HDFS_DISKSTORE_NAME2, HDFS_DISKDIR_NAME2, HDFS_STORE_NAME2);
            } else {
                dropAll(conn, tablename, null);
                createStores(conn, props, HDFS_DISKSTORE_NAME, HDFS_DISKDIR_NAME, HDFS_STORE_NAME);
            }

            if (generatequerydata) {
                createTable(conn, props, tablename, HDFS_STORE_NAME, null);
                if (generatetwotables) {
                    createTable(conn, props, tablename2, HDFS_STORE_NAME2, tablename);
                }
            }
        } catch (SQLException e) {
            System.err.println("Error in creating table(s). " + e);
            System.exit(1);
        } finally {
            if (conn != null) {
                System.out.println(getDateTimeStamp() + "Closing database connection.");
                conn.close();
            }
        }
    }

    public static String getDateTimeStamp() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        return "[info " + dateFormat.format(cal.getTime()) + "]";
    }
}